FROM node:22-alpine as base
WORKDIR /
COPY package*.json .
RUN npm install
COPY . .
EXPOSE 3000
HEALTHCHECK --interval=2s --timeout=1s --retries=1 --start-period=30s \
    CMD wget -nv -t1 --spider 'http://localhost:3000/' || exit 1
ENTRYPOINT ["npm","run","dev"]